###### wii安装priiloader0.9的问题

进入HBC后运行priiloader0.9安装程序出现提示：

cIOSPAGHETTI(Cios Infected) IOS detected

Press A to exit back to loader...

解决办法：PC上使用NUSD下载官方IOS58(旁边包含红点的IOS表示是系统关键部分，如果操作不当，Wii会损坏)。

Database-->IOS-->IOS58-->v6175

勾选Pack WAD，点击Start NUS Download!，开始下载。

使用wadmanager安装下载好的IOS58-64-v6175.wad。