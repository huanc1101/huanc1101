### PSV使用RetroArch多机种模拟器

一，下载RetroArch.vpk和Bundle压缩包

二，使用*VitaShell*传输RetroArch.vpk到PSV的`ux0:`，然后安装。安装完成后运行RetroArch，会发现字体和图片丢失。

三，解压Bundle并复制所有文件到`data/retroarch/`目录下，之后可正常运行RetroArch

PS: 进游戏后使用L R Select Start 组合键调出主菜单，游戏中保存后需要调出主菜单关闭游戏才能正常保存游戏。

RetroArch官网[PlayStation Vita - Libretro Docs](https://docs.libretro.com/guides/install-psv/)