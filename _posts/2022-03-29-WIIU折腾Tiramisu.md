### WIIU折腾Tiramisu

详细步骤访问https://wiiu.guide/

##### 所需文件

最新文件[https://tiramisu.foryour.cafe](https://tiramisu.foryour.cafe/)

选择 `Nanddumper`, `PayloadLoader Installer` and `Base-Tiramisu`.

The [01_sigpatches.rpx](https://wiiu.hacks.guide/docs/files/01_sigpatches.rpx) file.

##### 方法

解压Tiramisu.zip得到的文件放在SD卡根目录

复制 `01_sigpatches.rpx` 文件到 SD卡的`/wiiu/environments/tiramisu/modules/setup` .

SD卡装入WIIU主机，通过浏览器访问 `wiiuexploit.xyz`.

点击 `Run Homebrew Launcher!` 并按住 X button 打开Environment Loader menu（如果死机就重启主机，重置浏览器）

进入 `installer` 界面，把PayloadLoader安装进the Health and Safety Information app

##### 更改默认启动项

按住 Gamepad的(+) 键开机，可进入选择界面

##### vWii在USB接口运行wbfs

USB必须为主机平放时后面上方的口，我使用**Configurable USB-Loader Mod**才能正常识别USB口的U盘
