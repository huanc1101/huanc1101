## 安装retrogame使用GPIO按键控制

###### 安装

1下载retrogame.sh文件

```
curl -O https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/retrogame.sh
```

2

```
sudo bash retrogame.sh
```

运行后1~9选择安装类型，建议选择1。安装完成后选择Y，重启

###### 按键映射修改

按钮已预先映射，如果要重新映射控件，则需要修改`/boot/retrogame.cfg` 文件。

###### 按键测试

```
evtest
```

选择任何编号为 retrogame 的数字

retropie系统上的使用方法

使用键盘按“start”（之前定义为该键的按键）键进入retropie的设置菜单，选择“CONFIGURE INPUT”，使用连接GPIO的按键代替键盘进行retropie安装设置。

GPIO按键模拟为键盘的按键。

###### 网页无法访问的处理方法

修改hosts

```
sudo nano /etc/hosts
```

添加

```
199.232.68.133    raw.githubusercontent.com
```

