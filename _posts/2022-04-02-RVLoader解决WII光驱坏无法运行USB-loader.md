## RVLoader解决WII光驱坏无法运行USB-loader

详情访问[RVLoader v1.5 | BitBuilt - Giving Life to Old Consoles](https://bitbuilt.net/forums/index.php?threads/rvloader-v1-5.4295/)

推荐4.3纯净IOS安装RVLoader，避免未知问题。

###### 安装RVLoader

HBC进入RVLoader，进行安装操作

需要读取稳定的读卡器，避免启动死机

###### 卸载RVLoader

按住reset开机进入priiloader，修改启动菜单为SystemMenu，重启进入官方界面，把RVLoader安装的CIOS重新安装为官方版

###### wiiMoto手柄相关

必须在官方界面配对好手柄，可在RVLoader界面正常使用。

按住B键的同时按方向键来选择进入不同的功能菜单，选中后不松手等光标闪烁才能进入选中界面。

加载WII游戏前，每个游戏都需要单独设置打开蓝牙和NGC手柄模拟wiiMoto手柄，否则wiiMoto无法正常连接主机。